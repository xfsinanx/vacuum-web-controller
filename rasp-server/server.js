import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import primusInstance from './server/lib/primus';

const app = express();

const server = http.createServer(app);

primusInstance.connectPrimus();

const port = process.env.PORT || 5000;

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(bodyParser.json());

app.use('/test', (req, res) => res.status(200).send(`
  <!DOCTYPE html>
  <html>
  <head>
    <title></title>
  </head>
  <body>
    Hello
  </body>
  </html>
`));

server.listen(port, () => {
  console.log( `Server app started on port ${ port }` ) // eslint-disable-line
});

export default server;
