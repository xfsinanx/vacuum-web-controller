import { ORIENTATION } from '../lib/constants';

export const moveTo = (side) => {
  switch (side) {
    case ORIENTATION.LEFT.value:
      console.log('Someone told me to move LEFT');
      break;
    case ORIENTATION.RIGHT.value:
      console.log('Someone told me to move RIGHT');
      break;
    case ORIENTATION.TOP.value:
      console.log('Someone told me to move TOP');
      break;
    case ORIENTATION.BOTTOM.value:
      console.log('Someone told me to move BOTTOM');
      break;
    case ORIENTATION.STOP.value:
      console.log('Someone told me to STOP');
      break;
    default:
      console.log('DEFAULT IT IS');
  }
};
