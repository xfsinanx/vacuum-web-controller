import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
// import mongoose from 'mongoose';
import primusInstance from './server/lib/primus';
// import UserRoutes from './server/routes/user.routes';

const app = express();

const server = http.createServer(app);

primusInstance.connectPrimus(server);

// mongoose.connect('mongodb://vacuum-user-1:vacuum-user-1@ds121182.mlab.com:21182/vacuum-cleaner', {
//   useNewUrlParser: true,
// });

// mongoose.connection.on('error', (err) => {
//   // eslint-disable-next-line
//   console.warn(`Database error: ${err}`);
// });
//
// mongoose.connection.on('connected', () => {
//   // eslint-disable-next-line
//   console.log('Successfully connected to Database');
// });

const port = process.env.PORT || 8080;

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(bodyParser.json());

// app.use('/api/', UserRoutes);

app.use('/test', (req, res) => res.status(200).send(`
  <!DOCTYPE html>
  <html>
  <head>
    <title></title>
  </head>
  <body>
    Hello
  </body>
  </html>
`));

server.listen(port, () => {
  console.log( `Server app started on port ${ port }` ) // eslint-disable-line
});

export default server;
