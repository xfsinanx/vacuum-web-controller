import express from 'express';

import { getUserFromToken, getUserFromTokenIfHas } from '../helpers/TokenAuth';
import * as UserController from '../controllers/user.controller';

const router = express.Router();

router.route('/testget')
  .get(UserController.TestGet);

export default router;
