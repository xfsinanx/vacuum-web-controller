import jwt from 'jsonwebtoken';
import User from '../models/user';

const sendForbidden = res => res.status(401).send({ message: 'You are not verified' });

export const getUserFromToken = async (req, res, next) => {
  const header = req.headers.Authorization || req.headers.authorization;

  if (!header) { return sendForbidden(res); }

  const token = header.split('JWT ')[1];

  if (!token) { return sendForbidden(res); }
  try {
    const data = await jwt.verify(token, 'secretkey');
    const user = await User.findOne({ _id: data._id });
    req.user = user;
    return next();
  } catch (e) {
    return sendForbidden(res);
  }
};

export const getUserFromTokenIfHas = async (req, res, next) => {
  const header = req.headers.Authorization || req.headers.authorization;

  if (!header) { return next(); }

  return getUserFromToken(req, res, next);
};

export const getToken = async (user) => {
  const token = await jwt.sign({ _id: user._id }, 'secretkey');
  return token;
};
