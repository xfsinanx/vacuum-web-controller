import Primus from 'primus';
import Rooms from 'primus-rooms';
import PrimusEmit from 'primus-emit';

class PrimusService {
  async connectPrimus (server) {
    // this.primus.save(__dirname +'/primuslib.js');
    this.server = server || this.server;
    this.sparks = {};
    this.waitingSparks = [];
    this.vacuum = {
      id: 5,
      sparkId: null,
    };
    this.primus = new Primus(server, { transformer: 'websockets' });
    this.primus.plugin('rooms', Rooms);
    this.primus.plugin('emit', PrimusEmit);

    this.handleEvents();
  }

  handleEvents () {
    this.primus.on('connection', (spark) => {
      console.log('primus connection established...spark id: ', spark.id, spark.query);// eslint-disable-line


      this.primus.on('disconnection', (disSpark) => {
        const waitingSparksIndex = this.waitingSparks.indexOf(disSpark.id);
        if (waitingSparksIndex > -1) {
          this.waitingSparks.splice(waitingSparksIndex, 1);
          return;
        }
        if (disSpark.id === this.vacuum.sparkId) {
          const sparks = Object.keys(this.sparks);
          sparks.map(item => this.primus.spark(this.sparks[item]).emit('vaccum:leave', { message: 'Vacuum Leave' }));
          return;
        }
        const sparkIndex = Object.keys(this.sparks).find(userId => this.sparks[userId] === disSpark.id);
        if (this.waitingSparks.length > 0) {
          this.sparks[sparkIndex] = this.waitingSparks[0];
          this.waitingSparks.splice(0, 1);
          this.primus.spark(this.sparks[sparkIndex]).emit('vacuum:taken', { isTaken: false });
        } else {
          delete this.sparks[sparkIndex];
        }
        this.primus.spark(this.vacuum.sparkId).emit('vacuum:go', { orientation: 'stop' });
      });

      spark.on('join:vacuum', ({ userId }) => {
        if (userId === this.vacuum.id) {
          this.vacuum.sparkId = spark.id;
          return;
        }
        if (!this.sparks[userId]) {
          this.sparks[userId] = spark.id;
          return;
        }
        this.waitingSparks.push(spark.id);
        this.primus.spark(spark.id).emit('vacuum:taken', { isTaken: true });
      });

      spark.on('move:vacuum', ({ orientation }) => {
        if (this.waitingSparks.indexOf(spark.id) > -1) {
          this.primus.spark(spark.id).emit('vacuum:taken', { isTaken: true });
          return;
        }
        this.primus.spark(this.vacuum.sparkId).emit('vacuum:go', { orientation });
      });
    });
  }
}

const primusInstance = new PrimusService();

export default primusInstance;
