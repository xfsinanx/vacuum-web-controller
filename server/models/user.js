import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const { Schema } = mongoose;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  salt: {
    type: String,
  },
});

UserSchema.pre('save', function preSave (next) {
  if (this.password && this.isModified('password')) {
    this.salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, this.salt);
  }
  next();
});

UserSchema.methods.authenticate = function authenticate (password) {
  return this.password === bcrypt.hashSync(password, this.salt);
};

UserSchema.methods.removeSensitiveData = function () {
  delete this._doc.salt;
  delete this._doc.password;

  return this._doc;
};

export default mongoose.model('User', UserSchema);
