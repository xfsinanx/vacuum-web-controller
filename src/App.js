import React, { Component } from 'react';

import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './store/store';

import './style/manifest.css';
import Routes from './routes';

const composeEnhancers = composeWithDevTools({
  hostname: 'localhost',
});

export const store = createStore(
  reducers,
  composeEnhancers(
    applyMiddleware(ReduxThunk),
  ),
);


class App extends Component {
  render () {
    return (
      <div>
      <Provider store={store}>
        <Routes />
      </Provider>
      </div>
    );
  }
}

export default App;
