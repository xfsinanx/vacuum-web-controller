import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Arrow from './Arrow';
import { ORIENTATION } from '../../lib/constants';

class Joystick extends Component {
  render () {
    const { isTaken } = this.props;
    const hasTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
    return (
      <div className='Joystick'>
        {isTaken
          && <div>
            Joystick is taken
          </div>
        }
        <div className='JoystickContainer'>
          <Arrow
            hasTouch={hasTouch} onMouseDown={this.handleOnArrowMouseDown}
            onMouseUp={this.handleOnArrowMouseUp} orientation={ORIENTATION.TOP.value}
          />
          <div className='Joystick__leftRight'>
            <Arrow
            hasTouch={hasTouch} onMouseDown={this.handleOnArrowMouseDown}
            onMouseUp={this.handleOnArrowMouseUp} orientation={ORIENTATION.LEFT.value}
            />
            <Arrow
            hasTouch={hasTouch} onMouseDown={this.handleOnArrowMouseDown}
            onMouseUp={this.handleOnArrowMouseUp} orientation={ORIENTATION.RIGHT.value}
            />
          </div>
          <Arrow
            hasTouch={hasTouch} onMouseDown={this.handleOnArrowMouseDown}
            onMouseUp={this.handleOnArrowMouseUp} orientation={ORIENTATION.BOTTOM.value}
          />
        </div>
      </div>
    );
  }

  handleOnArrowMouseDown = (orientation) => {
    console.log(orientation);
    this.props.moveTo(orientation);
  }

  handleOnArrowMouseUp = () => {
    this.props.moveTo(ORIENTATION.STOP.value);
  }
}

Joystick.propTypes = {
  isTaken: PropTypes.bool,
  moveTo: PropTypes.func,
};

export default Joystick;
