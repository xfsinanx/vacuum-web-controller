import React from 'react';
import PropTypes from 'prop-types';

import ArrowImage from '../../assets/arrow.png';

const Arrow = ({
  onMouseDown, onMouseUp, orientation, hasTouch,
}) => (
  hasTouch
    ? (<div
  className={`ArrowContainer ${orientation}`}
  onTouchEnd={onMouseUp} onTouchStart={() => onMouseDown(orientation)}
    >
      <img src={ArrowImage} />
      {
        // onMouseDown={onMouseDown} onMouseUp={() => onMouseUp(orientation)}
      }
    </div>)
    : (
      <div
      className={`ArrowContainer ${orientation}`}
      onMouseDown={() => onMouseDown(orientation)} onMouseUp={onMouseUp}
      >
          <img src={ArrowImage} />
          {
            //
          }
        </div>
    )
);

Arrow.propTypes = {
  hasTouch: PropTypes.bool,
  onMouseDown: PropTypes.func,
  onMouseUp: PropTypes.func,
  orientation: PropTypes.string.isRequired,
};


export default Arrow;
