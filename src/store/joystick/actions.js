import PrimusService from '../../lib/PrimusService';

export const JOYSTICK = {
  TEST: 'TEST',
  JOYSTICK_TAKEN: 'JOYSTICK_TAKEN',
};

export const TestPrimus = orientation => (dispatch) => {
  PrimusService.testGetPrimus(orientation);
};

export const moveTo = orientation => () => {
  PrimusService.moveVacuum(orientation);
};

export const controllerOwnerChange = isTaken => (dispatch) => {
  dispatch({ type: JOYSTICK.JOYSTICK_TAKEN, payload: { isTaken } });
};
