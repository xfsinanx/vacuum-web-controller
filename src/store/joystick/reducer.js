import { JOYSTICK } from './actions';

export const INITIAL_STATE = {
  isTaken: false,
};

export default (state = INITIAL_STATE, action) => {
  const { payload } = action;
  switch (action.type) {
    case JOYSTICK.JOYSTICK_TAKEN:
      return { ...state, isTaken: payload.isTaken };
    default:
      return state;
  }
};
