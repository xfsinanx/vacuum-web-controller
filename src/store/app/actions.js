export const APP = {
  INIT: 'INIT',
};

export const initAction = () => (dispatch) => {
  dispatch({ type: APP.INIT, payload: { init: true } });
};
