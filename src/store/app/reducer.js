import { APP } from './actions';

export const INITIAL_STATE = {
  init: false,
};

export default (state = INITIAL_STATE, action) => {
  const { payload } = action;
  switch (action.type) {
    case APP.INIT:
      return { ...state, init: payload.init };
    default:
      return state;
  }
};
