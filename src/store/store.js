import { combineReducers } from 'redux';

import app from './app/reducer';
import joystick from './joystick/reducer';

export default combineReducers({
  app,
  joystick,
});
