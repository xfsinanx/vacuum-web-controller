export const ORIENTATION = {
  LEFT: {
    value: 'left',
  },
  RIGHT: {
    value: 'right',
  },
  TOP: {
    value: 'top',
  },
  BOTTOM: {
    value: 'bottom',
  },
  STOP: {
    value: 'stop',
  },
};
