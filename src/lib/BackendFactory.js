import Vacuum from './Vacuum';
import { appAuthToken } from './AppAuthToken';

export default function BackendFactory () {
  const token = appAuthToken.getSessionToken();
  Vacuum.initialize(token && token.token);
  return Vacuum;
}
