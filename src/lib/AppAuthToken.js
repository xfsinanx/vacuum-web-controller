import config from './config';

export class AppAuthToken {
  constructor () {
    this.SESSION_TOKEN_KEY = config.SESSION_TOKEN_KEY;
  }

  storeSessionToken (token) {
    window.localStorage.setItem(this.SESSION_TOKEN_KEY, JSON.stringify({ token }));
  }

  deleteSessionToken () {
    window.localStorage.removeItem(this.SESSION_TOKEN_KEY);
  }

  getSessionToken () {
    if (window.localStorage.getItem(this.SESSION_TOKEN_KEY)) {
      return JSON.parse(window.localStorage.getItem(this.SESSION_TOKEN_KEY));
    }
    return null;
  }
}

export const appAuthToken = new AppAuthToken();
