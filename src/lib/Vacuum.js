import axios from 'axios';

import config from './config';

class Vacuum {
  initialize (token) {
    this.API_BASE_URL = config.VACUUM.apiUrl;
    this.APP_BASE_URL = config.VACUUM.appUrl;

    this.sessionToken = token;
  }


  async _fetch (method, endpoint, body) {
    const constructedUrl = `${this.API_BASE_URL}${endpoint}`;
    const headers = this.sessionToken ? { Authorization: this.sessionToken } : {};
    try {
      const response = await axios({
        method,
        url: constructedUrl,
        headers: { ...headers, 'Content-Type': 'application/json' },
        data: body,
      });
      if (response.status >= 200 || response.status <= 299) {
        return response.data;
      }

      throw (response);
    } catch (error) {
      console.error(`Error fetching ${method}: ${endpoint}`, error.response.data);// eslint-disable-line
      throw new Error(error);
    }
  }
}

export default new Vacuum();
