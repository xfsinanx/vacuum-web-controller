import Primus from './primuslib';
import config from './config';
import { store } from '../App';
import { controllerOwnerChange } from '../store/joystick/actions';

class PrimusServiceBase {
  connectPrimus () {
    if (!this.primus) {
      try {
        this.primus.destroy();
      } catch (err) {
        console.warn('error destroying primus', err);//eslint-disable-line
      }
    }
    const endpoint = config.VACUUM.apiUrl;

    this.me = { _id: 1 };

    this.primus = new Primus(endpoint, {
      reconnect: {
        max: 5000,
        min: 500,
        retries: 50,
        'reconnect timeout': 2500,
      },
    });
    // this.subscribed = []
    this.handleEvents();
  }

  handleEvents () {
    this.primus.on('open', () => {
      if (this.reconnect) {
        clearInterval(this.reconnect);
      }

      this.primus.emit('join:vacuum', { userId: this.me._id });


      this.primus.on('vaccum:leave', () => {
        console.log('VACUUM LEAVED');
      });

      this.primus.on('vacuum:taken', (data) => {
        console.log('VACUUM TAKEN:', data);
        store.dispatch(controllerOwnerChange(data.isTaken));
      });
      // this.primus.emit('test:subscribe', { userId: this.me._id });

      // Send request to join the news room
      // this.primus.write({ action: 'join', room: 'news' });
      //
      // // Send request to leave the news room
      // this.primus.write({ action: 'leave', room: 'news' });
      //
      // this.primus.on('test:response', (data) => {
      //   console.log('RESPONSE IS:', data);//eslint-disable-line
      // });
      //
      // this.primus.on('data', (message) => {
      //   console.log(message);//eslint-disable-line
      // });
      //
      // this.primus.on('vacuum:back', ({ message }) => {
      //   store.dispatch(controllerOwnerChange(true));
      //   console.log('MESSAGE IS:', message);//eslint-disable-line
      // });
      //
      // this.primus.on('vacuum:taken', (data) => {
      //   store.dispatch(controllerOwnerChange(false));
      //   console.log('DATA OF VACUUM TAKEN IS:', data);//eslint-disable-line
      // });
    });
  }


  moveVacuum (orientation) {
    this.primus.emit('move:vacuum', { orientation });
  }

  testGetPrimus (orientation, userId = 1, randomUser = 5) {
    this.primus.emit('test:get-me', { userId, randomUser, orientation });
    console.log('SENDED');//eslint-disable-line
  }
}

const PrimusService = new PrimusServiceBase();

export default PrimusService;
