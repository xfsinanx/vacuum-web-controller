import React from 'react';
import PropTypes from 'prop-types';

import {
  Route,
} from 'react-router-dom';
import { appAuthToken } from '../lib/AppAuthToken';

export const isLoggedIn = () => !!appAuthToken.getSessionToken();

export const PrivateRoute = ({ component: Component, ...rest }) => (
  isLoggedIn() ? (
    <Route
      {...rest}
      render={
        props => (<Component {...props} />)
      }
    />
  ) : null// (<Redirect to='/' />)
);

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
};

export const GuestRoute = ({ component: Component, ...rest }) => (
  !isLoggedIn() ? (
    <Route
      {...rest}
      render={
        props => (<Component {...props} />)
      }
    />
  ) : null// (<Redirect to='/' />)
);

GuestRoute.propTypes = {
  component: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
};
