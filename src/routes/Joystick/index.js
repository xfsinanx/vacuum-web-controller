import { connect } from 'react-redux';

import { moveTo } from '../../store/joystick/actions';

import Joystick from '../../components/Joystick';

const mapStateToProps = ({ app: { init }, joystick: { isTaken } }) => ({ init, isTaken });

export default connect(mapStateToProps, { moveTo })(Joystick);
