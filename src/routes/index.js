import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';

import Joystick from './Joystick';
import PrimusService from '../lib/PrimusService';
import { routes } from '../lib/routes';

class Routes extends Component {
  render () {
    console.log(this.props.store);
    PrimusService.connectPrimus();
    return (
      <Router>
        <Switch>
          <Route component={Joystick} path={routes.JOYSTICK.path} />
        </Switch>
      </Router>
    );
  }
}

Routes.propTypes = {
};

export default connect(null, null)(Routes);
